<?php
// DSL all-in-one site.
include("../engine.php");

function ui_init() {
	echo "
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset='utf-8'>
		<meta http-equiv='X-UA-Compatible' content='IE=edge'>
		<meta name='viewport' content='width=device-width, initial-scale=1'>
		<title>[DSL] Disney Song Lovers</title>
		<link rel='shortcut icon' type='image/x-icon' href='favicon.ico'>
		<style type='text/css'>
			#content { max-width: 800px; padding: 0px 20px; margin: auto; }
			.navbar .fa { min-width: 20px; }
			.welcome { text-align: center; margin: 50px 0px; }
			.form-control { margin-bottom: 10px; }
			.gallery .block { height: 140px; background-size: cover; background-position: center center; background-repeat: no-repeat; position: relative; }
			.gallery .block .title { position: absolute; bottom: 0px; left: 0px; right: 0px; background-color: rgba(0, 0, 0, 0.75); color: white; padding: 2px 4px; white-space: nowrap; overflow: hidden; text-overflow: ellipsis; }
			.viewer { width: 100%; max-height: 1000px; text-align: center; position: relative; }
			.viewer img { max-height: 100%; max-width: 100%; box-shadow: 0px 0px 6px gray; }
			.btn-left { text-align: left; }
			.btn-right { text-align: right; }
			.gallery-nav { margin-top: 15px; margin-bottom: 100px; }
		</style>
		<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css' />
		<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/yeti/bootstrap.min.css' />
		<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css'>
		<link rel='stylesheet' href='dist/ui/trumbowyg.min.css'>
		<script src='https://code.jquery.com/jquery-1.11.3.min.js'></script>
		<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'></script>
		<script src='dist/trumbowyg.min.js'></script>
		<script src='dist/moment.min.js'></script>
	</head>
	<body>
		<nav class='navbar navbar-default'>
			<div class='container-fluid'>
				<div class='navbar-header'>
					<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#navbar' aria-expanded='true'>
						<span class='sr-only'>Toggle navigation</span>
						<span class='icon-bar'></span>
						<span class='icon-bar'></span>
						<span class='icon-bar'></span>
					</button>
					<a class='navbar-brand' href='?p=Home'><img src='dist/disney-song-lovers.png' style='width: 24px; display: inline;' /> [DSL] Disney Song Lovers</a>
				</div>
				<div class='navbar-collapse collapse out' id='navbar' aria-expanded='true'>
					<ul class='nav navbar-nav'>
						<li><a href='?p=Gallery'><span class='fa fa-picture-o'></span> Gallery</a></li>
						<li><a href='?p=Roster'><span class='fa fa-users'></span> Members</a></li>
						<li><a href='?p=Guides'><span class='fa fa-book'></span> Guides</a></li>
						<li><a href='?p=Contact'><span class='fa fa-envelope'></span> Contact</a></li>
						<li><a href='?p=TS3'><span class='fa fa-microphone'></span> TeamSpeak</a></li>
						<li><a href='http://disneysonglovers.enjin.com'><span class='fa fa-comment'></span> Forum </a></li>
					</ul>
				</div>
			</div>
		</nav>
		<div id='content' class='site-wrapper'>
	";
}

function ui_exit() {
	echo "
		</div>
	</body>
	</html>
	";
}

ui_init();
$page = "Home";
if (isset($_GET["p"])) $page = $_GET["p"];
switch ($page) {
	case "Gallery":
		if (isset($_POST["poster"])) {
			$target = "data/";
			$file = uniqid() . "_" . sha1($_POST["poster"]) . "_" . time() . "." . fext($_FILES["file"]["name"]);
			if (getimagesize($_FILES["file"]["tmp_name"])) {
				if(move_uploaded_file($_FILES["file"]["tmp_name"], $target . $file)) {
					iTF($target . $file, "thumb/" . $file, 300, 91);
					$gDB = readDB("gallery.db");
					$gDB[] = array("name" => $_POST["poster"], "file" => "{$file}", "desc" => $_POST["desc"]);
					writeDB("gallery.db", $gDB);
					echo "Added!";
				}
				else {
					echo "Failed to upload file!";
				}
			}
			else {
				echo "Invalid file!";
			}
		}
		echo "
		<h1>Share your guild experience</h1>
		<div class='welcome' style='margin: 10px;'>
			<img src='dist/disney-song-lovers-bar.png' style='height: 48px;' />
		</div>
		<form action='?p=Gallery' method='post' class='form-group well well-lg' enctype='multipart/form-data'>
			<h4>Have any screenshots or pictures to share?</h4>
			<input type='text' name='poster' placeholder='Your Name' class='form-control' />
			<input type='text' name='desc' placeholder='Description' class='form-control' />
			<input type='file' name='file' class='form-control' />
			<input type='submit' value='Share!' class='form-control' />
			<small>By uploading your image you agree to publicly post your image to this website.</small>
		</form>
		<hr />
		<h1>Gallery</h1>
		<h5>our story so far...</h5>
		<br />
		<div class='gallery row'>
		";
		$gDB = readDB("gallery.db");
		foreach ($gDB as $i => $data) {
			echo "
			<div class='col-md-4 block' style='background-image: url(thumb/{$data['file']});' onclick=\"location.href = '?p=GalleryView&index={$i}';\">
				<span class='title'>{$data['desc']}</span>
			</div>
			";
		}
		echo "
		</div>
		";
	break;
	case "GalleryView":
		$gDB = readDB("gallery.db");
		$data = $gDB[$_GET["index"]];
		echo "
		<h1>{$data['desc']}</h1>
		<h5>by {$data['name']}</h5>
		<div class='viewer'>
			<img src='data/{$data['file']}' />
		</div>
		<div class='row gallery-nav'>
		";
		if ($_GET["index"] > 0) {
			$i = $_GET["index"] - 1;
			$data = $gDB[$i];
			echo "
			<div class='col-md-6 btn-left'>
				<a href='?p=GalleryView&index={$i}' class='btn btn-primary'><span class='fa fa-arrow-left'></span> {$data['desc']}</a>
			</div>
			";
		}
		else {
			echo "
			<div class='col-md-6 btn-left'>
				&nbsp;
			</div>
			";
		}
		if ($_GET["index"] < count($gDB) - 1) {
			$i = $_GET["index"] + 1;
			$data = $gDB[$i];
			echo "
			<div class='col-md-6 btn-right'>
				<a href='?p=GalleryView&index={$i}' class='btn btn-primary'>{$data['desc']} <span class='fa fa-arrow-right'></span></a>
			</div>
			";
		}
		else {
			echo "
			<div class='col-md-6 btn-right'>
				&nbsp;
			</div>
			";
		}
		echo "
		</div>
		";
	break;
	case "ViewProfile":
		$pDB = readDB("profile.db");
		echo "
		<h1>My Profile</h1>
		<h4>{$_GET['aid']}</h4>
		<div class='welcome' style='margin: 10px;'>
			<img src='dist/disney-song-lovers-bar.png' style='height: 48px;' />
		</div>
		";
		if (isset($pDB[$_GET["aid"]])) {
			$p = file_get_contents("data/{$_GET['aid']}.profile");
			echo $p;
			echo "
			<br />
			<br />
			<hr />
			<p><small>Are you {$_GET['aid']}? Edit your profile <a href='?p=CreateProfile&aid={$_GET['aid']}'>here</a>.</small></p>
			";
		}
		else {
			echo "This profile has not been created yet. <a href='?p=CreateProfile&aid={$_GET['aid']}'>Create now!</a>";
		}
	break;
	case "CreateProfile":
		$pDB = readDB("profile.db");
		if (isset($_POST["txt-content"])) {
			if (isset($pDB[$_GET["aid"]]) && $pDB[$_GET["aid"]] !== "" && (!isset($_GET["pid"]) || $pDB[$_GET["aid"]] !== $_GET["pid"])) {
				echo "
				<script>
					location.href = '?p=CreateProfile&aid={$_GET['aid']}';
				</script>
				";
			}
			else {
				$pDB[$_GET["aid"]] = $_POST["password"];
				writeDB("profile.db", $pDB);
				file_put_contents("data/{$_GET['aid']}.profile", $_POST["txt-content"]);
				echo "
				<div class='alert alert-success'>
					<strong>Thank you!</strong> Your profile has been updated. If you set a password, you will need to use that password to edit your profile.
				</div>
				<a href='?p=ViewProfile&aid={$_GET['aid']}' class='btn btn-xs btn-primary'>View my profile</a>
				";
			}
		}
		elseif (isset($pDB[$_GET["aid"]]) && $pDB[$_GET["aid"]] !== "" && (!isset($_GET["pid"]) || $pDB[$_GET["aid"]] !== $_GET["pid"])) {
			echo "
			<h2>Protected Profile</h2>
			<hr />
			<b>Please enter your password to edit your profile.</b>
			<br />
			<br />
			<form action='?' method='get' class='form-group'>
				<input type='hidden' name='p' value='CreateProfile' />
				<input type='hidden' name='aid' value='{$_GET['aid']}' />
				<input type='text' name='pid' class='form-control' placeholder='Password' />
				<input type='submit' value='Save' class='form-control' />
			</form>
			";
		}
		else {
			$pid = "";
			$pid2 = "";
			if (isset($_GET["pid"])) {
				$pid = "&pid={$_GET['pid']}";
				$pid2 = $_GET["pid"];
			}
			$c = "";
			if (isset($pDB[$_GET["aid"]])) {
				$c = file_get_contents("data/{$_GET['aid']}.profile");
			}
			echo "
			<h1>New Profile</h1>
			<h4>for {$_GET['aid']}</h4>
			<div class='welcome' style='margin: 10px;'>
				<img src='dist/disney-song-lovers-bar.png' style='height: 48px;' />
			</div>
			<b>Write a short bio about yourself about whatever you want.</b>
			<form action='?p=CreateProfile&aid={$_GET['aid']}{$pid}' method='post' class='form-group'>
				<div class='form-control' id='txt-content'>{$c}</div>
				<input type='text' name='password' placeholder='(optional) enter a password to protect your profile so others cannot edit it' class='form-control' value='{$pid2}' />
				<br />
				<b>Your bio will be available to be viewed publicly, so please do not post any sensitive information that you do not want others to see.</b>
				<br />
				<br />
				<input type='submit' value='Save' class='form-control' />
			</form>
			<script>
				$('#txt-content').trumbowyg();
			</script>
			";
		}
	break;
	case "Roster":
		if (isset($_POST["role"])) {
			$mDB = readDB("member.db");
			$mDB[$_POST["role"]][$_POST["account"]] = array("name" => $_POST["name"], "extra" => $_POST["extra"]);
			writeDB("member.db", $mDB);
		}
		$mDB = readDB("member.db");
		echo "
		<h1>Members</h1>
		<div class='welcome' style='margin: 10px;'>
			<img src='dist/disney-song-lovers-bar.png' style='height: 48px;' />
		</div>
		<table class='table table-striped table-hover'>
			<tbody>
		";
		
		$roleList = array(
			"leader" => "Leader / Co-Leader",
			"dungeon" => "Dungeon Master",
			"fractal" => "Fractal Master",
			"map" => "Map Master",
			"pvp" => "PvP Master",
			"jumping" => "Jumping Master",
			"officer" => "Officers",
			"member" => "Members",
		);
		
		foreach ($roleList as $id => $role) {
			echo "<tr><th colspan='5'>{$role}</th></tr>";
			if (isset($mDB[$id]) && count($mDB[$id]) > 0) {
				foreach ($mDB[$id] as $account => $data) {
					echo "<tr><td><a href='?p=ViewProfile&aid={$account}'><span class='fa fa-user'></span> view profile</a></td><td style='font-weight: bold;'>{$data['name']}</td><td>{$account}</td><td>{$data['extra']}</td></tr>";
				}
			}
		}
		
		echo "
			</tbody>
		</table>
		<hr />
		<h2>Are you a [DSL] guildie?</h2>
		<form action='?p=Roster' method='post' class='form-group well well-lg'>
			<select name='role' class='form-control'>
				<option>I am a...</option>
				<option value='leader'>Leader / Co-Leader</option>
				<option value='dungeon'>Dungeon Master</option>
				<option value='fractal'>Fractal Master</option>
				<option value='map'>Map Master</option>
				<option value='pvp'>PvP Master</option>
				<option value='jumping'>Jumping Master</option>
				<option value='officer'>Officer</option>
				<option value='member'>Member</option>
			</select>
			<input type='text' name='name' placeholder='Your (Preferred) Name' class='form-control' />
			<input type='text' name='account' placeholder='Your Account Name.1234' class='form-control' />
			<input type='text' name='extra' placeholder='(optional) Extra Information' class='form-control' />
			<input type='submit' value='Add me!' class='form-control' />
			<small>Wrong information? Contact [kuruharu.9780] or email <a href='mailto:admin@aftermirror.com'>admin@aftermirror.com</a> to get it fixed.<br />If you want to update your information, just enter the same account name and your new information.</small>
		</form>
		";
	break;
	case "Guides":
		if (isset($_GET["msg"])) {
			switch ($_GET["msg"]) {
				case "success":
					echo "
					<div class='alert alert-success'>
						<strong>Thank you!</strong> The guide has been saved successfully. Thank you very much for contributing.
					</div>
					";
				break;
				case "failed":
					echo "
					<div class='alert alert-danger'>
						<strong>Oops!</strong> Something went wrong.
					</div>
					";
				break;
			}
		}
		echo "
		<h1>Guides</h1>
		<h5>These guides are free to edit by anyone. Help [DSL] create a large assortment of guides for current and future guildies!</h5>
		<div class='welcome' style='margin: 10px;'>
			<img src='dist/disney-song-lovers-bar.png' style='height: 48px;' />
		</div>
		<a href='?p=CreateGuide' class='btn btn-primary'>Create a new guide</a>
		<hr />
		";
		$uDB2 = readDB("guide.db");
		$atoz = array();
		$uDB = array();
		foreach ($uDB2 as $uid => $data) {
			$atoz[$uid] = $data["title"];
		}
		natsort($atoz);
		foreach (array_keys($uDB2) as $uid) {
			$uDB[$uid] = $uDB2[$uid];
		}
		
		foreach ($uDB as $uid => $data) {
			switch ($data["group"]) {
				case "general": echo "General "; break;
				case "fractaldungeon": echo "Fractal/Dungeon "; break;
				case "jumping": echo "Jumping "; break;
				case "money": echo "Money "; break;
				case "maplevel": echo "Map/Level "; break;
			}
			
			if ($data["name"] !== $data["original_name"]) {
				echo "<a href='?p=ViewGuide&uid={$uid}'><b>{$data['title']}</b> by {$data['original_name']} - updated by {$data['name']}</a><br/>";
			}
			else {
				echo "<a href='?p=ViewGuide&uid={$uid}'><b>{$data['title']}</b> by {$data['name']}</a><br/>";
			}
		}
	break;
	case "CreateGuide":
		if (isset($_GET["uid"])) {
			$uDB = readDB("guide.db");
			$data = $uDB[$_GET["uid"]];
			
			$gg = "";
			$fg = "";
			$jg = "";
			$mg = "";
			$lg = "";
			switch ($data["group"]) {
				case "general": $gg = " checked"; break;
				case "fractaldungeon": $fg = " checked"; break;
				case "jumping": $jg = " checked"; break;
				case "money": $mg = " checked"; break;
				case "maplevel": $lg = " checked"; break;
			}
			
			$c = file_get_contents("data/{$_GET['uid']}.guide");
			echo "
			<h1>Guide Editor</h1>
			<h5>Want to update an existing guide?</h5>
			<hr />
			<form action='?p=UpdateGuide&uid={$_GET['uid']}' method='post' class='form-group'>
				<label for='title'>Title</label>
				<input type='text' name='title' id='title' placeholder='A short title for your guide' class='form-control' value='{$data['title']}' />
				<label for='group'>Guide Type</label>
				<select name='group' id='group' class='form-control'>
					<option value='general'{$gg}>General Guide</option>
					<option value='fractaldungeon'{$fg}>Fractal / Dungeon Guide</option>
					<option value='jumping'{$jg}>Jumping Puzzle Guide</option>
					<option value='money'{$mg}>Money Making Tips</option>
					<option value='maplevel'{$lg}>Mapping / Leveling Guide</option>
				</select>
				<label for='name'>Your Name?</label>
				<b><small>Please change the name field even if you are not the original author.</small></b>
				<input type='text' name='name' id='name' placeholder='So we know who updated this awesome guide!' class='form-control' value='{$data['name']}' />
				<hr />
				<div class='form-control' id='txt-content'>{$c}</div>
				<input type='submit' value='Save' class='form-control' />
			</form>
			<script>
				$('#txt-content').trumbowyg();
			</script>
			";
		}
		else {
			echo "
			<h1>Guide Editor</h1>
			<h5>What do you want to write?</h5>
			<hr />
			<form action='?p=UpdateGuide' method='post' class='form-group'>
				<label for='title'>Title</label>
				<input type='text' name='title' id='title' placeholder='A short title for your guide' class='form-control' />
				<label for='group'>Guide Type</label>
				<select name='group' id='group' class='form-control'>
					<option value='general'>General Guide</option>
					<option value='fractaldungeon'>Fractal / Dungeon Guide</option>
					<option value='jumping'>Jumping Puzzle Guide</option>
					<option value='money'>Money Making Tips</option>
					<option value='maplevel'>Mapping / Leveling Guide</option>
				</select>
				<label for='name'>Your Name?</label>
				<input type='text' name='name' id='name' placeholder='So we know who made this awesome guide!' class='form-control' />
				<hr />
				<div class='form-control' id='txt-content'></div>
				<input type='submit' value='Save' class='form-control' />
			</form>
			<script>
				$('#txt-content').trumbowyg();
			</script>
			";
		}
	break;
	case "UpdateGuide":
		$uDB = readDB("guide.db");
		if (isset($_POST["title"])) {
			if (isset($_GET["uid"])) {
				// update
				$uid = $_GET["uid"];
				$uDB[$uid]["title"] = $_POST["title"];
				$uDB[$uid]["group"] = $_POST["group"];
				$uDB[$uid]["name"] = $_POST["name"];
				$uDB[$uid]["date_modified"] = time();
				writeDB("guide.db", $uDB);
				file_put_contents("data/{$uid}.guide", $_POST["txt-content"]);
				echo "<h2>Successfully updated guide list</h2>";
			}
			else {
				$uid = sha1(uniqid());
				$uDB[$uid] = array(
					"title" => $_POST["title"],
					"group" => $_POST["group"],
					"name" => $_POST["name"],
					"original_name" => $_POST["name"],
					"date_created" => time(),
					"date_modified" => time()
				);
				writeDB("guide.db", $uDB);
				file_put_contents("data/{$uid}.guide", $_POST["txt-content"]);
				echo "<h2>Successfully updated guide list</h2>";
			}
			echo "
			<script>
				location.href = '?p=Guides&msg=success';
			</script>
			";
		}
		else {
			echo "
			<script>
				location.href = '?p=Guides&msg=failed';
			</script>
			";
		}
	break;
	case "ViewGuide":
		$uDB = readDB("guide.db");
		$data = $uDB[$_GET["uid"]];
		echo "
		<div class='row'>
			<div class='col-sm-9'>
				<h1>{$data['title']}</h1>
		";
		if ($data["name"] !== $data["original_name"]) {
			echo "<h5>originally by {$data['original_name']}, last revised by {$data['name']}</h5>";
		}
		else {
			echo "<h5>submitted by {$data['name']}</h5>";
		}
		echo "
			</div>
			<div class='col-sm-3' style='text-align: right;'>
				<br />
				<a href='?p=CreateGuide&uid={$_GET['uid']}' class='btn btn-sm btn-primary'>Edit / Update Guide</a>
			</div>
		</div>
		<div class='welcome' style='margin: 0;'>
			<img src='disney-song-lovers-bar.png' style='height: 48px;' />
		</div>
		<div>
		";
		echo file_get_contents("data/{$_GET['uid']}.guide");
		echo "
		</div>
		";
	break;
	case "Contact":
		echo "
		<h1>Contacting the guild</h1>
		<div class='welcome' style='margin: 0;'>
			<img src='dist/disney-song-lovers-bar.png' style='height: 48px;' />
		</div>
		<br />
		<p>Are you interested in joining the guild or perhaps getting in touch with [DSL]?</p>
		<br />
		<br />
		<p>Feel free to in-game message any of the officers of [DSL] found in the <a href='?p=Roster'>Members</a> page.</p>
		<hr />
		<br />
		<h2>About the website &mdash; kuru!</h2>
		<br />
		<h4>Hello, nice to meet you, I'm kuru!</h4>
		<p>You may or may not have seen me around the guild but I'm usually playing on my character, Ifia.<br /><br />I am currently a second year college student majoring in Computer Science. Besides playing Guild Wars 2, I like to do web development in my spare time.</p>
		";
	break;
	case "TS3":
		echo "
		<h1>Do you want to... sing along?</h1>
		<div class='welcome' style='margin: 10px;'>
			<img src='dist/disney-song-lovers-bar.png' style='height: 48px;' />
		</div>
		<h2>What is TeamSpeak?</h2>
		<p>TeamSpeak is a free software that you can download from <a href='https://www.teamspeak.com/downloads'>teamspeak.com</a>.</p>
		<h2>Do I need...?</h2>
		<h4>Do I need a microphone?</h4>
		<p>No, microphones are not required.</p>
		<h4>Why should I do this?</h4>
		<p>It helps with dungeon runs and stuff, but joining our TeamSpeak is not required. Some people prefer normal chat which is completely understandable.</p>
		<h2>TeamSpeak Connection Guide</h2>
		<strong>If you want to chat with us or would like to join our TeamSpeak:</strong>
		<ol>
			<li>Download TeamSpeak using the link above.</li>
			<li>You will have to set up TeamSpeak for the first time. This includes microphone configuration if your computer has a microphone connected to it.</li>
			<li>In order to join [DSL]'s TeamSpeak, enter 'dsl.aftermirror.com' as the server you wish to join.</li>
			<li>We ask that you keep your username on TeamSpeak slightly relevant to your name (account, in-game, or given name) on Guild Wars 2, but it is not required.</li>
		</ol>
		";
	break;
	case "Forum":
	break;
	case "Home":
		echo "
		<div class='welcome'>
			<img src='dist/disney-song-lovers-lg.png' style='width: 128px;' /><br/>
			<h1>Welcome to [DSL]</h1>
			<h4>home of the Disney Song Lovers</h4>
		</div>
		<div class='list-group'>
			<h4 class='list-group-item-heading'>Hello there!</h4>
			<p class='list-group-item-text'>Feel free to look around the site. If something breaks on the website or TeamSpeak, please do not hesitate to contact [kuruharu.9780]. For guild inqueries, please go to the contact page to look for active guild leaders.</p>
		</div>
		<hr />
		<h4>Calendar</h4>
		<table class='table table-striped table-hover'>
			<thead>
				<tr>
					<th>Date</th>
					<th colspan='2'>Event</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Saturday</td>
					<td><em>approximately</em> <b>10:00 PM - 12:00 AM (CST)</b> Guild Missions</td>
					<td id='cal_gm_timer'>--</td>
				</tr>
			</tbody>
		</table>
		<br/>
		<a href='?p=EventTimer' class='btn btn-primary'>Event Timer</a>
		<script>
			$(function() {
				$('#cal_gm_timer').text(moment().day(6).fromNow());
			});
		</script>
		";
	break;
	case "EventTimer":
		echo "
		<h1>Event Timer</h1>
		<div class='welcome' style='margin: 10px;'>
			<img src='dist/disney-song-lovers-bar.png' style='height: 48px;' />
		</div>
		<div style='text-align: center;'>
			<div style='width: 235px; overflow: hidden; margin: auto;'>
				<iframe src='http://guildwarstemple.com/dragontimer/embeddragons.php?serverKey=119&langKey=en' width='255' height='570' scrolling='yes' frameborder='0'><p>Your browser does not support iframes.</p></iframe>
			</div>
			<br />
			<a href='http://guildwarstemple.com/dragontimer/events.php?serverKey=119&langKey=1' class='btn btn-primary btn-sm'>Timer from guildwarstemple.com</a>
		</div>
		";
	break;
	default:
		echo "
		<h1>Error!</h1>
		<div class='alert alert-danger'>
			<strong>Oh no!</strong> We could not find the page you were looking for.<br/>If you followed a valid link, please contact [kuruharu.9780] so he can go fix it.
		</div>
		";
	break;
}
ui_exit();
?>